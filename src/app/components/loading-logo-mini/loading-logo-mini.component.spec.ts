import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingLogoMiniComponent } from './loading-logo-mini.component';

describe('LoadingLogoMiniComponent', () => {
  let component: LoadingLogoMiniComponent;
  let fixture: ComponentFixture<LoadingLogoMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingLogoMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingLogoMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
