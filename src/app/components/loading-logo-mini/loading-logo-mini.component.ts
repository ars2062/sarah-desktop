import { Component, OnInit } from '@angular/core';
declare const SVG:any;

@Component({
  selector: 'app-loading-logo-mini',
  templateUrl: './loading-logo-mini.component.html',
  styleUrls: ['./loading-logo-mini.component.scss']
})
export class LoadingLogoMiniComponent implements OnInit {
  draw = SVG('#LogoMini').size(300,300);
  constructor() { }

  ngOnInit() {
  }

}
