import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingLogoMiniComponent } from './loading-logo-mini/loading-logo-mini.component';



@NgModule({
  declarations: [LoadingLogoMiniComponent],
  imports: [
    CommonModule
  ],
  exports:[
    LoadingLogoMiniComponent
  ]
})
export class ComponentsModule { }
