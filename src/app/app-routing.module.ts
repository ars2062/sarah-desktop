import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { DeviceComponent } from './views/device/device.component';
import { LoginComponent } from './views/login/login.component';
import { PluginComponent } from './views/plugin/plugin.component';
import { SettingComponent } from './views/setting/setting.component';


const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'device',
    component:DeviceComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'plugin',
    component:PluginComponent
  },
  {
    path:'setting',
    component:SettingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
