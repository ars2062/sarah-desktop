import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DeviceComponent } from './device/device.component';
import { PluginComponent } from './plugin/plugin.component';
import { SettingComponent } from './setting/setting.component';
import { ComponentsModule } from '../components/components.module';



@NgModule({
  declarations: [HomeComponent, LoginComponent, DeviceComponent, PluginComponent, SettingComponent],
  imports: [
    CommonModule,
    ComponentsModule
  ],
  exports:[
    DeviceComponent,
    HomeComponent,
    LoginComponent,
    PluginComponent,
    SettingComponent
  ]
})
export class ViewsModule { }
